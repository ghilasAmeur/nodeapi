//console.log('nodemon marche bien mieux que avant sans le nodemon ...!!')
// 1- telecharement de paquet express et nodemon
// 2- crier la BDD mysql meteo_node avec une table meteo_node_1 avec : id, ville, temperature, decription
// 3- ajouter le paquet mysql apres le telechargement npm install mysql, et crier une connection

const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const routeGlobal = require("./routes/routes");
const routeUser = require("./routes/routeUser");
// const auth = require('./token');
// const connection = require('./connectionMeteo');

//nous permet de partager nos donnes nimport ou pas de blocage
const cors = require("cors");
// const routeMeteo = require('./routeMeteo')
// const routePost = require('./routePost')
// const routeDelete = require('./routeDelete')
// const routeLogin = require('./routeLogin')
// const auth = require('./token')

app.use(bodyParser.json());
app.use(cors());

app.use("/meteo", routeGlobal);
app.use("/auth", routeUser);

app.listen(3001, () => {
  console.log("je suis sur le port  3001");
});

// app.use('/login', routeLogin);
// app.use('/meteo', routeMeteo);
// //pour ajouter une meteo a la bdd
// app.use('/post', routePost );
// //pour supprimer la meteo de la bdd
// app.use('/delete', routeDelete);

//recuperer * des data de la BDD
/*
app.get('/meteo', (req, res) =>{

        connection.query('SELECT * FROM meteo_node_1', (err, data) => {
            //console.log(req.body)
            //ici il fallait que je parse les data car j'arrive pas a recuperer ville ou un seul objet avec console.log(data)
            //j'avais console.log(data) remplace par :  data =  JSON.parse(JSON.stringify(data));
            if (!err) {

                data = JSON.parse(JSON.stringify(data));
                res.send(data)
                console.log(data[0].ville);
            }
            else console.log(err);

        });

});
*/

//ici /meteo/nom_de_ville et renvoie la temperature de la ville passé en param  :

// app.get('/meteo/:ville', (req, res) => {

//     //console.log(req.params);
//     const ville = req.params.ville;

//     sql = 'SELECT * FROM meteo_node_1 WHERE ville = ?';

//     connection.query(sql, [ville], (err, data) => {

//         data = JSON.parse(JSON.stringify(data));
//         console.log(data[0].ville)
//         //res.send(data) attaentin a la repetion de res.send ca cause des problrmes, il ma causé un prblm
//         if (!err) res.send(" La température de " + ville + " est:" + data[0].temperature);
//         else console.log(err);
//     });
// });

//recuperer les data avec id en paramaetre :
//ici j'avais un prblm j'arrive pas a recuperer de la bdd car il faut parser pour int sinon ca marche pas !! tres important
// res.send(req.params.id);

///////////////:id////////////////////////////////////////////

// app.get('/meteo/:id', (req, res) => {

//  const id = parseInt(req.params.id);

//   const sql = 'SELECT * FROM meteo_node_1 WHERE id = ?';
//   connection.query(sql, [id], (err, data) =>{

//       if (!err) res.send(data);
//       else console.log(err);
//   });

//  });

///////////////:id////////////////////////////////////////////
//update avec put
//  app.put('/:id', (req, res) =>{

//      const id = parseInt(req.params.id);
//      const sql = 'SELECT * FROM meteo_node_1 WHERE id = ?';
//      connection.query(sql, [id], (err, data) =>{

//         if(!err) {
//             ville = req.body.ville;
//             temperature = req.body.temperature;
//             description = req.body.description;
//             // console.log(req.body);
//             res.send(data)
//         }
//         else console.log(err);
//      })

//  });

//AJOUTER  des data de la BDD

// app.post('/', (req, res) =>{

//     var sql = 'INSERT INTO meteo_node_1 (ville, temperature, description) VALUES ("Toulouse", 23, " Beau temps à Toulouse")';
//     connection.query(sql, (err, data) => {
//         if (!err) {
//             console.log(data.json);

//         }
//         else console.log("Erreur est :" + err);
//     })

// });
