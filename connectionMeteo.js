
const mysql = require('mysql');

const connection = mysql.createConnection({
    
    host : 'localhost',
    user: 'root',
    password: '',
    database: 'meteo_node'

});

connection.connect((err) =>{
    if(!err) console.log('Connection réussi à la BDD')
    else console.log(err)
    });

    module.exports = connection;
