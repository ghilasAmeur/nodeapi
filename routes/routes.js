const express = require("express");
const route = express.Router();

//const connection = require('../connectionMeteo');
const meteo = require("../controllers/controllers");
const auth = require("../token");

route.get("/", meteo.afficher_Meteo);

route.get("/:ville", meteo.afficher_Meteo_ville);

route.post("/", meteo.ajouter_Meteo);

route.delete("/:id", meteo.supprimer_Meteo);

//    route.put('/:id',meteo.update_Meteo);

module.exports = route;
